package main

import (
	"encoding/json"
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/xorm"
	"gitlab.com/btlike/database/torrent"
)

type tor struct {
	Infohash   string
	Name       string
	CreateTime time.Time
	Length     int64
	FileCount  int64
}

func main() {
	engine, err := xorm.NewEngine("mysql", "root:yanyuanld2046@tcp(10.99.0.12:3306)/torrent?charset=utf8")
	if err != nil {
		panic(err)
	}

	var ct torrent.Infohash
	count, err := engine.Count(&ct)
	if err != nil {
		panic(err)
	}
	for i := 2650000; i < int(count); i += 10000 {
		fmt.Println("start", i, time.Now())
		var cts []torrent.Infohash
		err = engine.Limit(10000, i).Find(&cts)
		if err != nil {
			fmt.Println(err)
			return
		}

		var (
			i0 []torrent.Infohash0
			i1 []torrent.Infohash1
			i2 []torrent.Infohash2
			i3 []torrent.Infohash3
			i4 []torrent.Infohash4
			i5 []torrent.Infohash5
			i6 []torrent.Infohash6
			i7 []torrent.Infohash7
			i8 []torrent.Infohash8
			i9 []torrent.Infohash9
			ia []torrent.Infohasha
			ib []torrent.Infohashb
			ic []torrent.Infohashc
			id []torrent.Infohashd
			ie []torrent.Infohashe
			iF []torrent.Infohashf
		)

		for _, v := range cts {
			var t tor
			err = json.Unmarshal([]byte(v.Data), &t)
			if err != nil {
				// fmt.Println(err)
				continue
			}

			if t.Name == "" {
				continue
			}

			switch v.Id[0] {
			case '0':
				var data torrent.Infohash0
				data.CreateTime = t.CreateTime
				data.Data = v.Data
				data.Infohash = t.Infohash
				i0 = append(i0, data)
			case '1':
				var data torrent.Infohash1
				data.CreateTime = t.CreateTime
				data.Data = v.Data
				data.Infohash = t.Infohash
				i1 = append(i1, data)
			case '2':
				var data torrent.Infohash2
				data.CreateTime = t.CreateTime
				data.Data = v.Data
				data.Infohash = t.Infohash
				i2 = append(i2, data)
			case '3':
				var data torrent.Infohash3
				data.CreateTime = t.CreateTime
				data.Data = v.Data
				data.Infohash = t.Infohash
				i3 = append(i3, data)
			case '4':
				var data torrent.Infohash4
				data.CreateTime = t.CreateTime
				data.Data = v.Data
				data.Infohash = t.Infohash
				i4 = append(i4, data)
			case '5':
				var data torrent.Infohash5
				data.CreateTime = t.CreateTime
				data.Data = v.Data
				data.Infohash = t.Infohash
				i5 = append(i5, data)
			case '6':
				var data torrent.Infohash6
				data.CreateTime = t.CreateTime
				data.Data = v.Data
				data.Infohash = t.Infohash
				i6 = append(i6, data)
			case '7':
				var data torrent.Infohash7
				data.CreateTime = t.CreateTime
				data.Data = v.Data
				data.Infohash = t.Infohash
				i7 = append(i7, data)
			case '8':
				var data torrent.Infohash8
				data.CreateTime = t.CreateTime
				data.Data = v.Data
				data.Infohash = t.Infohash
				i8 = append(i8, data)
			case '9':
				var data torrent.Infohash9
				data.CreateTime = t.CreateTime
				data.Data = v.Data
				data.Infohash = t.Infohash
				i9 = append(i9, data)
			case 'A':
				var data torrent.Infohasha
				data.CreateTime = t.CreateTime
				data.Data = v.Data
				data.Infohash = t.Infohash
				ia = append(ia, data)
			case 'B':
				var data torrent.Infohashb
				data.CreateTime = t.CreateTime
				data.Data = v.Data
				data.Infohash = t.Infohash
				ib = append(ib, data)
			case 'C':
				var data torrent.Infohashc
				data.CreateTime = t.CreateTime
				data.Data = v.Data
				data.Infohash = t.Infohash
				ic = append(ic, data)
			case 'D':
				var data torrent.Infohashd
				data.CreateTime = t.CreateTime
				data.Data = v.Data
				data.Infohash = t.Infohash
				id = append(id, data)
			case 'E':
				var data torrent.Infohashe
				data.CreateTime = t.CreateTime
				data.Data = v.Data
				data.Infohash = t.Infohash
				ie = append(ie, data)
			case 'F':
				var data torrent.Infohashf
				data.CreateTime = t.CreateTime
				data.Data = v.Data
				data.Infohash = t.Infohash
				iF = append(iF, data)
			default:
				fmt.Println(t.Infohash)
			}
		}
		count, err := engine.Insert(&i0)
		if err != nil {
			fmt.Println(err)
		}

		count, err = engine.Insert(&i1)
		if err != nil {
			fmt.Println(err)
		}

		count, err = engine.Insert(&i2)
		if err != nil {
			fmt.Println(err)
		}

		count, err = engine.Insert(&i3)
		if err != nil {
			fmt.Println(err)
		}

		count, err = engine.Insert(&i4)
		if err != nil {
			fmt.Println(err)
		}

		count, err = engine.Insert(&i5)
		if err != nil {
			fmt.Println(err)
		}

		count, err = engine.Insert(&i6)
		if err != nil {
			fmt.Println(err)
		}

		count, err = engine.Insert(&i7)
		if err != nil {
			fmt.Println(err)
		}

		count, err = engine.Insert(&i8)
		if err != nil {
			fmt.Println(err)
		}

		count, err = engine.Insert(&i9)
		if err != nil {
			fmt.Println(err)
		}

		count, err = engine.Insert(&ia)
		if err != nil {
			fmt.Println(err)
		}

		count, err = engine.Insert(&ib)
		if err != nil {
			fmt.Println(err)
		}

		count, err = engine.Insert(&ic)
		if err != nil {
			fmt.Println(err)
		}

		count, err = engine.Insert(&id)
		if err != nil {
			fmt.Println(err)
		}

		count, err = engine.Insert(&ie)
		if err != nil {
			fmt.Println(err)
		}

		count, err = engine.Insert(&iF)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(count)
	}
}
