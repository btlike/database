package torrent

type Infohash struct {
	Id   string `xorm:"not null pk default '' VARCHAR(40)"`
	Data string `xorm:"VARCHAR(1024)"`
}
